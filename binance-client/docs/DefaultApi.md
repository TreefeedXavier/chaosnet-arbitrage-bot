# swagger_client.DefaultApi

All URIs are relative to *https://dex.binance.org/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_account**](DefaultApi.md#get_account) | **GET** /v1/account/{address} | Get an account.
[**get_closed_orders**](DefaultApi.md#get_closed_orders) | **GET** /v1/orders/closed | Get closed orders.
[**get_depth**](DefaultApi.md#get_depth) | **GET** /v1/depth | Get the order book.
[**get_open_orders**](DefaultApi.md#get_open_orders) | **GET** /v1/orders/open | Get open orders.

# **get_account**
> AccountDetail get_account(address)

Get an account.

Gets account metadata for an address.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
address = 'address_example' # str | The account address to query

try:
    # Get an account.
    api_response = api_instance.get_account(address)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_account: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | **str**| The account address to query | 

### Return type

[**AccountDetail**](AccountDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_closed_orders**
> OrderDetail get_closed_orders(address, start=start, end=end, limit=limit, offset=offset, side=side, symbol=symbol)

Get closed orders.

Gets closed (filled and cancelled) orders for a given address.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
address = 'address_example' # str | the owner address
start = 789 # int | start time in miliseconds (optional)
end = 789 # int | end time in miliseconds (optional)
limit = 56 # int | default 500; max 1000 (optional)
offset = 56 # int | start with 0, default 0 (optional)
side = 56 # int | order side. 1 for buy and 2 for sell (optional)
symbol = 'symbol_example' # str | Market pair symbol (optional)

try:
    # Get closed orders.
    api_response = api_instance.get_closed_orders(address, start=start, end=end, limit=limit, offset=offset, side=side, symbol=symbol)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_closed_orders: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | **str**| the owner address | 
 **start** | **int**| start time in miliseconds | [optional] 
 **end** | **int**| end time in miliseconds | [optional] 
 **limit** | **int**| default 500; max 1000 | [optional] 
 **offset** | **int**| start with 0, default 0 | [optional] 
 **side** | **int**| order side. 1 for buy and 2 for sell | [optional] 
 **symbol** | **str**| Market pair symbol | [optional] 

### Return type

[**OrderDetail**](OrderDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_depth**
> DepthDetail get_depth(symbol, limit=limit)

Get the order book.

Gets the order book depth data for a given pair symbol.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
symbol = 'symbol_example' # str | Market pair symbol
limit = 56 # int | The limit of results (optional)

try:
    # Get the order book.
    api_response = api_instance.get_depth(symbol, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_depth: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **symbol** | **str**| Market pair symbol | 
 **limit** | **int**| The limit of results | [optional] 

### Return type

[**DepthDetail**](DepthDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_open_orders**
> OrderDetail get_open_orders(address, limit=limit, offset=offset, symbol=symbol)

Get open orders.

Get open orders.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
address = 'address_example' # str | the owner address
limit = 56 # int | default 500; max 1000 (optional)
offset = 56 # int | start with 0, default 0 (optional)
symbol = 'symbol_example' # str | Market pair symbol (optional)

try:
    # Get open orders.
    api_response = api_instance.get_open_orders(address, limit=limit, offset=offset, symbol=symbol)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_open_orders: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **address** | **str**| the owner address | 
 **limit** | **int**| default 500; max 1000 | [optional] 
 **offset** | **int**| start with 0, default 0 | [optional] 
 **symbol** | **str**| Market pair symbol | [optional] 

### Return type

[**OrderDetail**](OrderDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

