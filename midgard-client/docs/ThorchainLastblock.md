# ThorchainLastblock

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chain** | **str** |  | [optional] 
**lastobservedin** | **int** |  | [optional] 
**lastsignedout** | **int** |  | [optional] 
**thorchain** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

