from logger import get_logger, logging
from midgard import MidgardApi
from binance import BinanceApi
from thorchain import ThorApi
from calculator import clp, divide_conquer
from decimal import Decimal, ROUND_DOWN
import argparse
parser = argparse.ArgumentParser(description='Examples')
parser.add_argument("-k", "--key", type=str, required=True)
args = parser.parse_args()
private_key = args.key
# list of chaosnet seed: https://chaosnet-seed.thorchain.info/
# list of testnet seed : https://testnet-seed.thorchain.info/
testnet = '175.41.137.209:8080'
chaosnet = '18.159.173.48:8080'

# -------------------------------------- ChaosNet --------------------------------------
# # api connected to ThorNode Query
# thornode = ThorApi()
# # get a list of pool
# pools_info = thornode.get_pools()
# print(pools_info[0].balance_asset)
# get one pool
# pool_info = thornode.get_pool(pool='BNB.BUSD-BD1')
# asset_depth = float(pool_info.balance_asset) / 10**8
# rune_depth = float(pool_info.balance_rune) / 10**8
# # get rune_price for one pool
# price_rune = float(thornode.get_rune_price(pool='BNB.BNB'))
# # get tx_in (user->thorchain_vault) meants for finished transactions
# swap_tx_in = '894F495B40380369A610A1B6E6D6B1217A147EB70A3F4D4F22A0C3D507BB8ADB'
# # stake_tx shown as incomplete before withdrawl on thornode, which turns get_tx_status into a loop
# stake_tx = 'CDCE10AEA7722AAE1EECE3FB455ED0D091A6CADA373857199EE7BAF983EE288C'
# tx = thornode.get_tx_in(hash=swap_tx_in)
# # get boolean status of tx_in ('done' = True), optional argument {timeout}
# #status = thornode.get_tx_status(hash=swap_tx_in)
# # get vault address, optional argument {chain}
# address = thornode.get_vault(chain='BNB')
#
# midgard = MidgardApi()
# tx = midgard.get_txs_id(txid=stake_tx)

# # api connected to binance and is able to broadcast msg {default env: production}
# binance = BinanceApi(key=args.key, test=False)
# # get account balance information
# binance.account_info()
# binance.get_balance()
# thor_bnb_in = 1
# # swap operation
# limit = price_rune * thor_bnb_in * 0.7
# thor_hash = binance.thor_swap(chain='BNB', i_symbol='BNB', o_symbol=binance.RUNE, amount=thor_bnb_in, to_address=address, limit=limit)
# tx = midgard.get_txs_id(txid=thor_hash)
# #
# stake operation
# thor_rune_in = thor_bnb_in * price_rune
# thor_hash = binance.thor_stake(chain='BNB', symbol='BNB', amount=thor_bnb_in, runeamount=thor_rune_in, to_address=address)
# tx = thornode.get_tx_status(hash=thor_hash)
# tx = midgard.get_txs_id(txid=thor_hash)
# # withdraw operation
# thor_hash = binance.thor_withdraw(chain='BNB', symbol='BNB', percent=100, to_address=address)
# tx = midgard.get_txs_id(txid=thor_hash)


# -------------------------------------- TestNet --------------------------------------

# # api connected to Midgard {default host: chaosnet}
# midgard = MidgardApi(host=testnet)
# # get boolean status of network.catching_up
# status = midgard.health_check()
# # get global stats from midgard
# stats = midgard.get_stats()
# # get a list of assets basic information {list of assets separated by commas}
# asset = midgard.get_assets(assets='BNB.BNB')[0]
# price_rune = float(asset.price_rune)
# # get a list of pools
# pools = midgard.get_pools()
# # get detailed pools information {list of assets separated by commas}
# pool_info = midgard.get_pools_detail(assets='BNB.BNB')[0]
# asset_depth = float(pool_info.asset_depth) / 10**8
# rune_depth = float(pool_info.rune_depth) / 10**8
# # get vault address optional argument {chain}
# address = midgard.get_pools_address(chain='BNB')
# # get detailed txs information, used after transaction
# swap_tx_in = 'ADE5636FC6DC5BF9001B98912279D6B812032847E9C0F8A1A2A253CE7A792F7D'
# swap_tx_out = '31AC765CE5954B461141DF483987E1AAE4E95664E0321B16A48B38E462C45566'
# tx = midgard.get_txs_id(txid=swap_tx_in)
# tx = midgard.get_txs_id(txid=swap_tx_out)
#
# # api connected to binance and is able to broadcast msg {default env: production}
# binance = BinanceApi(key=args.key, test=True)
# # get account balance information
# binance.account_info()
# thor_bnb_in = 5
# # swap operation
# limit = price_rune * thor_bnb_in * 1.1
# thor_hash = binance.thor_swap(chain='BNB', i_symbol='BNB', o_symbol=binance.RUNE, amount=1, to_address=address, limit=limit)
# tx = midgard.get_txs_id(txid=thor_hash)
#
# # stake operation
# thor_rune_in = thor_bnb_in * price_rune
# thor_hash = binance.thor_stake(chain='BNB', symbol='BNB', amount=thor_bnb_in, runeamount=thor_rune_in, to_address=address)
# tx = midgard.get_txs_id(txid=thor_hash)
#
# # withdraw operation
# thor_hash = binance.thor_withdraw(chain='BNB', symbol='BNB', percent=100, to_address=address)
# tx = midgard.get_txs_id(txid=thor_hash)
# # smart swap operation
# thor_hashes = binance.thor_smart_swap(chain='BNB', i_symbol='BNB', o_symbol=binance.RUNE, amount=thor_bnb_in, to_address=address, slice=10, limit=limit)
# for hash in thor_hashes:
#     tx = midgard.get_txs_id(txid=hash)
